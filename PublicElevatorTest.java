public class PublicElevatorTest{

	public static void main(String [] args){

		PublicElevator pubElevator = new PublicElevator();
		pubElevator.bukaPintu = true; //Penumpang masuk
		pubElevator.bukaPintu = false;

	//Pergi ke lantai 0 dibawah gedung
	
		pubElevator.lantaiSkrng--;
		pubElevator.lantaiSkrng++;

	//Lompat ke lantai 7 (hanya ada 5 lantai dalam gedung)

		pubElevator.lantaiSkrng = 7;
		pubElevator.bukaPintu = true; //Penumpang masuk / keluar
		pubElevator.bukaPintu = false;
		pubElevator.lantaiSkrng = 1; //Menuju lantai pertama
		pubElevator.bukaPintu = true; //Penumpang masuk / keluar
		pubElevator.lantaiSkrng++; //Elevator bergerak tanpa menutup pintu
		pubElevator.bukaPintu = false;
		pubElevator.lantaiSkrng--;
		pubElevator.lantaiSkrng--;
	}
}