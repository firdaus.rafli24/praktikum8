public class PrivateElevatorTest{

	public static void main(String [] args){

		PrivateElevator privElevator = new PrivateElevator();
		privElevator.bukaPintu = true; //penumpang masuk
		privElevator.bukaPintu = false; //pintu ditutup
	
	//Pergi ke lantai 0 dibawah gedung
	
		privElevator.lantaiSkrng--;
		privElevator.lantaiSkrng++;

	//Lompat ke lantai 7 (hanya ada 5 lantai dalam gedung)

		privElevator.lantaiSkrng = 7;
		privElevator.bukaPintu = true; //Penumpang masuk / keluar
		privElevator.bukaPintu = false;
		privElevator.lantaiSkrng = 1; //Menuju lantai pertama
		privElevator.bukaPintu = true; //Penumpang masuk / keluar
		privElevator.lantaiSkrng++; //Elevator bergerak tanpa menutup pintu
		privElevator.bukaPintu = false;
		privElevator.lantaiSkrng--;
		privElevator.lantaiSkrng--;
	}
}