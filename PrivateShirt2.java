public class PrivateShirt2{

	private int idBaju = 0;
	private String keterangan = "-Keterangan diperlukan-";
	private char kodeWarna = 'U';
	private double harga = 0.0;
	private int jmlStock = 0;

	public char getkodeWarna(){

		return kodeWarna;
	}
	
	public void setkodeWarna(char kode){
	
		switch (kode){

			case 'R':
			case 'G':
			case 'B':

				kodeWarna = kode;
				break;
			default:
				System.out.println("Kode Warna Salah,gunakan R,G,atau B");
		}
	}
	
	public int getidBaju(){

		return idBaju;
	}

	public void setidBaju(int id){

		idBaju = id;

	}

	public String getKeterangan(){

		return keterangan;

	}
	
	public double gethargaBaju(){

		return harga;

	}

	public void sethargaBaju(double price){

		harga = price;

	}

	public int getjmlStock(){

		return jmlStock;
	}
}