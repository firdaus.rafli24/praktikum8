public class StudentRecord{
	
	private String name;
	private String address;
	private int age;
	private double mathGrade;
	private double englishGrade;
	private double scienceGrade;
	private double average;
	private static int STUDENTCOUNT;

	
//Konstruktor
	
	public StudentRecord(){
		
		STUDENTCOUNT++;
		
	}

	public StudentRecord(String temp){
	
		this.name = temp;
		STUDENTCOUNT++;
	}
	
	public StudentRecord(String name,String Address){
	
		this.name = name;
		this.address = Address;
		STUDENTCOUNT++;
	}		
	
	public StudentRecord(double mGrade,double eGrade,double sGrade){
	
		mathGrade = mGrade;
		englishGrade = eGrade;
		scienceGrade = sGrade;
		STUDENTCOUNT++;
	}

	/**
	* Menghasilkan nama dari siswa
	*/
	
	
	public String getName(){
	
		return name;
	}
	
	/**
	* Mengubah nama siswa
	*/
	
	public void setName(String temp){
		
		name = temp;
	}
	
	public String getAddress(){
	
		return address;
	}

	public void setAddress(String temp){
	
		address= temp;
	}
	
	public int getAge(){
	
		return age;
	}

	public void setAge(int temp){
	
		age = temp;
	}
	
	/**
	* Menghitung rata - rata nilai Matematika,Bahasa Inggris,* * Ilmu pasti
	*/

	public double getAverage(){
		
		double result = 0;
		result = (mathGrade+englishGrade+scienceGrade)/3;
		return result;
	}
	
	/**
	* Menghasilkan jumlah instance StudentRecord
	*/
	
	public static int getStudentCount(){
		
		return STUDENTCOUNT;
	}

	public void print(String temp){
	
		System.out.println("Name: "+name);
		System.out.println("Address: "+address);
		System.out.println("Age: "+age);
		System.out.println();
	}

	public void print(double mGrade,double eGrade,double sGrade){
		
		System.out.println("Name: "+name);
		System.out.println("Math Grade: "+mathGrade);
		System.out.println("English Grade: "+englishGrade);
		System.out.println("Science Grade: "+scienceGrade);
		System.out.println();
	}

	public double getEngGrade(){
	
		return englishGrade;
	}

	public void setEngGrade(double temp){

		englishGrade = temp;

	}

	public double getMathGrade(){

		return mathGrade;

	}

	public void setMathGrade(double temp){

		mathGrade = temp;
	
	}

	public double getSciGrade(){

		return scienceGrade;

	}

	public void setSciGrade(double temp){

		scienceGrade = temp;

	}
		
}