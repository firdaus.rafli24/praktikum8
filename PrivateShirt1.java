public class PrivateShirt1{

	private int idBaju = 0;
	private String keterangan = "-Keterangan diperlukan-";
	private char kodeWarna = 'U';
	private double harga = 0.0;
	private int jmlStock = 0;

	public char getkodeWarna(){

		return kodeWarna;
	}

	public void setkodeWarna(char kode){

		kodeWarna = kode;

	}

	public int getidBaju(){

		return idBaju;
	}

	public void setidBaju(int id){

		idBaju = id;

	}

	public String getKeterangan(){

		return keterangan;

	}

	public void setKeterangan(String temp){
	
		keterangan = temp;
	}
	
	public double gethargaBaju(){

		return harga;

	}

	public void sethargaBaju(double price){

		harga = price;

	}

	public int getjmlStock(){

		return jmlStock;
	}
}
